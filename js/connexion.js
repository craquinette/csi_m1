var btnInscription = $("#btnInscription");
var identique = $("#identiqueCache");
identique.hide();


$("#mdpI").change(() => {
   

    if ($("#confirmation").val() !== $("#mdpI").val()) {
        identique.show();
        btnInscription.prop('disabled', true);
    } else {
        identique.hide();
        btnInscription.prop('disabled', false);
    }
    
});

$("#confirmation").change(() => {
    if ($("#confirmation").val().length > 7) {
        if ($("#confirmation").val() !== $("#mdpI").val()) {
            identique.show();
            btnInscription.prop('disabled', true);
        } else {
            identique.hide();
            btnInscription.prop('disabled', false);
        }
    }
});