<?php
session_start();

include '../models/bddNews.php';
if(isset($_SESSION['pseudo']) && isset($_SESSION['pwd'])){
    Bdd::connectUser($_SESSION['pseudo'],$_SESSION['pwd']);
}else{
    header('Location: ../site/connexion.php'); // a vérifier
}
$bdd=Bdd::getBdd();

// MAJ texte news
if (isset($_POST['texte']) && isset($_POST['modifier'])){
    $newTexte = str_replace("'","\'",$_POST['texte']);
    $news = $bdd->update('news',array(array("texte",$newTexte)),array(array('idnews','=',$_GET['idnews'])));
    header('Location: ../site/lireNews.php?news=' .$_GET['idnews']);
}

// Suppression news
if (isset($_POST['supprimer'])){
    $news = $bdd->delete('news_mot_cle',array(array('idnews','=',$_GET['idnews'])));
    $news = $bdd->delete('news',array(array('idnews','=',$_GET['idnews'],'')));
    header('Location: ../site/accueil.php?info=suppr');
}

// Modification étude news
if (isset($_POST['validerEtude'])){
    $justificationEtude = htmlspecialchars($_POST['justification']);
    $justificationEtude=str_replace("'","\'",$justificationEtude);
    $categorieNews = htmlspecialchars($_POST['valideInvalide']);
    $news = $bdd->update('news',array(array("categorie",$categorieNews),array("justification_etude",$justificationEtude)),array(array('idnews','=',$_GET['idnews'])));
    header('Location: ../site/lireNews.php?news=' .$_GET['idnews']);
}
?>