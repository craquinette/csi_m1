<?php
session_start();
include '../models/bdd.php';
if(isset($_SESSION['pseudo']) && isset($_SESSION['pwd'])){
    Bdd::connectUser($_SESSION['pseudo'],$_SESSION['pwd']);
}else{
    header('Location: ../site/connexion.php'); // a vérifier
}
$bdd=Bdd::getBdd();

$justificationEtude = htmlspecialchars($_POST['justification']);
$justificationEtude=str_replace("'","\'",$justificationEtude);
$categorieNews = htmlspecialchars($_POST['valideInvalide']);
$idNewsAValider = $_POST['idnewsvalidee'];
$news = $bdd->update('news',array(array("categorie",$categorieNews),array("justification_etude",$justificationEtude)),array(array('idnews','=',$idNewsAValider)));
header('Location: ../site/accueil.php');
