<?php
session_start();
include '../models/bddNews.php';
if(isset($_SESSION['pseudo']) && isset($_SESSION['pwd'])){
    Bdd::connectUser($_SESSION['pseudo'],$_SESSION['pwd']);
}else{
    header('Location: ../site/connexion.php'); // a vérifier
}
$bdd=Bdd::getBdd();


if (isset($_POST['selectDomaine'])){
        $valeur = htmlentities($_POST['selectDomaine'], ENT_QUOTES, "UTF-8");
        $bdd->insert('abonne_domaine',array(array("iddomaine",$valeur),array("idabonne",$_SESSION['id'])));
        header('Location: ../site/monProfil.php');
        die();
}else if (isset($_POST['selectDomaineSuppr'])){
        $valeurASuppr = htmlentities($_POST['selectDomaineSuppr'], ENT_QUOTES, "UTF-8");
        $abonneDomaine = $bdd->delete('abonne_domaine',array(array('iddomaine','=',$valeurASuppr,'AND'),array('idabonne','=',$_SESSION['id'])));
        header('Location: ../site/monProfil.php');
        die();
}else if(isset($_POST['newLiblDomaine'])){
    $erreurNomDomaine = false;
    $liblDomaine = htmlspecialchars($_POST['newLiblDomaine']);
    $selectAllDomaine = $bdd->get('domaine');
    foreach ($selectAllDomaine as $valeurNewDomaine) {
        if(in_array($liblDomaine, $valeurNewDomaine)){
            $erreurNomDomaine = true;
        }
    }
    // Cas valide
    if(!$erreurNomDomaine){
        $bdd->insert('domaine',array(array("libelle",$liblDomaine),array("etat","En attente")));
        $redirection = 'Location: ../site/monProfil.php?msgDomaine=success';
        header($redirection);
        die();
    // Cas faux
    }else{
        $redirection = 'Location: ../site/monProfil.php?msgDomaine=fail';
        header($redirection);
        die();
    }
}


// MAJ INFOS ABONNE
// MAJ mdp
if (isset($_POST['password']) && $_POST['password'] != ''){
    if(strlen($_POST['password']) > 8 AND ($_POST['password'] == $_POST['password_confirm'])){
        $newPassword = $_POST['password'];
        //$abonnes = $bdd->update('abonne',array(array("mdp",$newPassword)),array(array('idabonne','=',$_SESSION['id'],'AND')));
        changerMdp($_SESSION['pseudo'],$_SESSION['pwd'],$_SESSION['id'],$newPassword,'true');
        $_SESSION['pwd']=$newPassword;
    }else{
        $redirection = 'Location: ../site/monProfil.php?msg=mdp';
        header($redirection);
        die();
    }
}

// MAJ telephone
if (isset($_POST['telephone'])){
    $newTelephone = htmlspecialchars($_POST['telephone']);
    $abonnes = $bdd->update('abonne',array(array("telephone",$newTelephone)),array(array('idabonne','=',$_SESSION['id'])));
}

// MAJ nom
if (isset($_POST['nom'])){
    $newNom = htmlspecialchars($_POST['nom']);
    $abonnes = $bdd->update('abonne',array(array("nom",$newNom)),array(array('idabonne','=',$_SESSION['id'],'AND')));
}

// MAJ prenom
if (isset($_POST['prenom'])){
    $newPrenom = htmlspecialchars($_POST['prenom']);
    $abonnes = $bdd->update('abonne',array(array("prenom",$newPrenom)),array(array('idabonne','=',$_SESSION['id'],'AND')));
}

// MAJ animal
if (isset($_POST['animal'])){
    $newAnimal = htmlspecialchars($_POST['animal']);
    $abonnes = $bdd->update('abonne',array(array("animal_compagnie",$newAnimal)),array(array('idabonne','=',$_SESSION['id'],'AND')));
}


// MAJ INFOS ADMIN
// MAJ MAIL
if (isset($_POST['mail'])){
    if(stristr($_POST['mail'], '@')){
        $newMail = htmlspecialchars($_POST['mail']);
        $admin = $bdd->update('administrateur',array(array("mail",$newMail)),array(array('idadministrateur','=',$_SESSION['id'])));
    }else{
        $redirection = 'Location: ../site/monProfil.php?msgadm=mail';
        header($redirection);
        die();
    }
}


// MAJ mdp
if (isset($_POST['passwordAdmin']) && $_POST['passwordAdmin'] != ''){
    if(strlen($_POST['passwordAdmin']) > 8 AND ($_POST['passwordAdmin'] == $_POST['password_confirm_admin'])){
        $newPassword = $_POST['passwordAdmin'];
        changerMdp($_SESSION['pseudo'],$_SESSION['pwd'],$_SESSION['id'],$newPassword,'false');
        $_SESSION['pwd']=$newPassword;
    }else{
        $redirection = 'Location: ../site/monProfil.php?msgadm=mdp';
        echo $redirection;
        header($redirection);
        die();
    }
}

// MAJ nb news
if (isset($_POST['nbNews'])){
    $newNbNews = htmlspecialchars($_POST['nbNews']);
    $admin = $bdd->update('administrateur',array(array("nombre_news",$newNbNews)),array(array('idadministrateur','=',$_SESSION['id'])));
}

// MAJ % publications validées
if (isset($_POST['percentPubli'])){
    $newPercentPubli = htmlspecialchars($_POST['percentPubli']);
    $admin = $bdd->update('administrateur',array(array("pourcentage_publication_validee",$newPercentPubli)),array(array('idadministrateur','=',$_SESSION['id'])));
}

// Retour sur monProfil à la fin des update
header('Location: ../site/monProfil.php');