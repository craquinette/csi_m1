<?php
    session_start();
    //include("./statusCheck.php");
    require "../models/bdd.php";
    if(isset($_SESSION['pseudo']) && isset($_SESSION['pwd'])){
        Bdd::connectUser($_SESSION['pseudo'],$_SESSION['pwd']);
    }else{
        header('Location: ../site/connexion.php'); // a vérifier
    }
    $bdd=Bdd::getBdd();

    if (isset($_POST['titre']) && isset($_POST['texteNews']) && isset($_POST['selectDomaine']) && isset($_POST['mots_cles1']) && isset($_POST['mots_cles2']) && isset($_POST['mots_cles3']) && isset($_POST['dureePubli'])) {
        $titre = $_POST['titre'];
        $texteNews = $_POST['texteNews'];
        $domaine = $_POST['selectDomaine'];
        $mot_cle1 = $_POST['mots_cles1'];
        $mot_cle2 = $_POST['mots_cles2'];
        $mot_cle3 = $_POST['mots_cles3'];
        $duree_publi = $_POST['dureePubli'];
        
        $titre=str_replace("'","\'",$titre);
        $texteNews=str_replace("'","\'",$texteNews);
        

        Bdd::connectSystem();
        $bdd = Bdd::getBdd();
        $bdd->insert('news',array(array("titre",$titre),array("texte",$texteNews),array("date_publication",date("Y-m-d")), array("categorie","non valide"), array("iddomaine",$domaine), array("idredacteur",$_SESSION['id']), array("nbjourpublication",$duree_publi)));
        Bdd::connectUser($_SESSION['pseudo'],$_SESSION['pwd']);
        $bdd = Bdd::getBdd();
        
        $news_id = $bdd->get('news',array('max(idnews)'));
        $news_id = $news_id[0][0];
        
        //Insert les trois mots-clés
        $bdd->insert('mot_cle',array(array("nom",$mot_cle1)));
        $idmotcle = $bdd->get('mot_cle',array('max(idmotcle)'));
        $idmotcle = $idmotcle[0][0];
        $bdd->insert('news_mot_cle',array(array("idnews",$news_id),array("idmotcle",$idmotcle)));
        
        $bdd->insert('mot_cle',array(array("nom",$mot_cle2)));
        $idmotcle = $bdd->get('mot_cle',array('max(idmotcle)'));
        $idmotcle = $idmotcle[0][0];
        $bdd->insert('news_mot_cle',array(array("idnews",$news_id),array("idmotcle",$idmotcle)));
        
        $bdd->insert('mot_cle',array(array("nom",$mot_cle3)));
        $idmotcle = $bdd->get('mot_cle',array('max(idmotcle)'));
        $idmotcle = $idmotcle[0][0];
        $bdd->insert('news_mot_cle',array(array("idnews",$news_id),array("idmotcle",$idmotcle)));
    }
    header('Location: ../site/accueil.php');
?>