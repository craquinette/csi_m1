<?php
session_start();
//include("./statusCheck.php");
require "../models/bddNews.php";

//Cas de la connexion
if (isset($_POST['loginC']) && isset($_POST['mdpC'])) {
    $pseudo = $_POST['loginC'];
    $mdp = $_POST['mdpC'];
    //faire finction connexion

    archivage();
    passageAbonneConfiance();

    $pseudo=strtolower($pseudo);

    $connect = connexion($pseudo, $mdp);
    //echo $connect;
    if ($connect !=0) {

        
        
        Bdd::connectUser($pseudo,$mdp);
        $bdd = Bdd::getBdd();


        //variable de session à ajouter
        $_SESSION['connected'] = $connect; //un statut dans ce cas


        
        if($connect ==2 || $connect ==3)
            $_SESSION['id'] = $bdd->get('abonne',array('idabonne'),null,array(array('pseudo','=',$pseudo,'AND')))[0]['idabonne']; //faire fonction trouver ID
        else
            $_SESSION['id'] = $bdd->get('administrateur',array('idadministrateur'),null,array(array('pseudo','=',$pseudo,'AND')))[0]['idadministrateur']; //faire fonction trouver ID
        $_SESSION['pseudo'] = $pseudo;
        $_SESSION['pwd'] = $mdp;



       header('Location: ../site/accueil.php');
    } else {
        //Cas mot de passe incorrect
        header('Location: ../site/connexion.php?loginerror=true');
    }
} else if(isset($_POST['loginI']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['tel']) && isset($_POST['mdpI']) && isset($_POST['animal'])){
    //Cas de l'inscription : on va toujours retouner sur la page connexion pour se connecter
    
    $pseudo = htmlspecialchars($_POST['loginI']);
    $pseudo=strtolower($pseudo);
    $nom = htmlspecialchars($_POST['nom']);
    $prenom = htmlspecialchars($_POST['prenom']);
    $animal = htmlspecialchars($_POST['animal']);
    $tel = htmlspecialchars($_POST['tel']);
    $password = htmlspecialchars($_POST['mdpI']);
    
    
    //$verifNom=existePseudo($pseudo);
   
    $verifNom=inscription($pseudo, $prenom, $nom, $tel, $password, $animal);
    if(!$verifNom){
        header('Location: ../site/connexion.php?erreur=true');
    }else{
        //session = verifNom
        header('Location: ../site/connexion.php');
    }
}else{
    //Cas ou il manque quelque chose
    deconnexion();
    header('Location: ../site/connexion.php');
}
