<?php
    include_once("header.php");
    if (isset($_SESSION['id']) AND ($_SESSION['connected'] == 2 OR $_SESSION['connected'] == 3)){
        // Problème : attendre qu'on ait la fonction de rechercher d'ID lors de la connexion
        $abonnes = $bdd->get('abonne',array(),array(),array(array('idabonne','=',$_SESSION['id'])));
        foreach ($abonnes as $value) {
            $pseudo = $value['pseudo'];
            $nom = $value['nom'];
            $prenom = $value['prenom'];
            $telephone = $value['telephone'];
            $animal = $value['animal_compagnie'];
        }
    }

    if (isset($_SESSION['id']) AND $_SESSION['connected'] == 1 ){
        $admin = $bdd->get('administrateur',array(),array(),array(array('idadministrateur','=',$_SESSION['id'])));
        foreach ($admin as $valueAdmin) {
            $pseudo = $valueAdmin['pseudo'];
            $mailAdmin = $valueAdmin['mail'];
            $nbNews = $valueAdmin['nombre_news'];
            $percentPubliNews = $valueAdmin['pourcentage_publication_validee'];
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mon profil</title>
        <link rel="stylesheet" type="text/css" href="../style/monprofil.css" />
    </head>
    
    <body>
        <header>
            <legend>
                <center>Bienvenue sur votre profil <?php echo $pseudo?> </center>
            </legend>  
        </header>
        <div name="infosAbonne">
            <?php if($_SESSION['connected'] == 2 OR $_SESSION['connected'] == 3){  ?>
                <div id='modifinfosperso' class ='jumbotron'>
                    <div>Modifier vos informations personnelles</div>

                    <form method="POST" action="../functions/traitementMonProfil.php">
                        
                    <div class="row">
                            
                        <div class="col"><label for="nom">Nom</label>
                            <input type="text" class="form-control" value=<?php echo $nom; ?> name="nom" id="nom"/>
                        </div>
                
                        <div class="col">
                            <label for="prenom" class="form-label">Prénom</label>
                            <input type="text" class="form-control form-control-sm" value=<?php echo $prenom; ?> name="prenom" id="prenom"/><br>
                        </div>
                       
                        <div class="col">
                            <label for="telephone" class="form-label">Numéro de téléphone</label>
                            <input type="text" class="form-control form-control-sm" minlength="10" maxlength="10" value=<?php echo $telephone; ?> name="telephone" id="telephone"/><br>
                        </div>
                        <div class="col">
                            <label for="animal" class="form-label">Animal de compagnie</label>
                            <input type="text" class="form-control form-control-sm" value=<?php echo $animal; ?> name="animal" id="animal"/><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="password" class="form-control form-control-sm" placeholder="Modifier votre mot de passe" name="password" id="password"/><br>
                        </div>
                        <div class="col">   
                            <input type="password" class="form-control form-control-sm" placeholder="Confirmation du mdp" name="password_confirm" id="password_confirm"/><br>
                        </div>
                    </div>
                    <input class="btn btn-outline-success" type="submit" name="submitbut" value="Modifier"/>
                        
                    </form>
                    
                    <?php
                        if(isset($_GET['msg'])){
                            if($_GET['msg'] == "tel"){
                                echo '<span>Le numéro de téléphone est faux</span>';
                            }else if($_GET['msg'] == "mdp"){
                                echo '<span>Vous avez une erreur dans le mot de passe</span>';
                            }
                        }
                    ?>
                </div>
                
                    <div name="domaineAbonne"  class ='jumbotron'>
                        <h3>Vos domaines préférés</h3>
                        <ul>
                        <?php
                            // Affichage des domaines préférés de l'abonné
                            $libelleDomaine = $bdd->get('domaine',array('libelle'),array(array('INNER','abonne_domaine','domaine.iddomaine','abonne_domaine.iddomaine')),array(array('abonne_domaine.idabonne','=',$_SESSION['id'])));
                            foreach($libelleDomaine as $valeurDomaine){
                                echo '<li>' .$valeurDomaine['libelle']. '</li>';
                            }
                        ?>
                        </ul>
                    </div>

                <div class="row">   
                    <div class='col jumbotron' >
                        <div>Choisir ses domaines d'intérêt</div>
                    
                        <?php
                        // On affiche les domaines uniquement validé par l'admin
                        $domaine = $bdd->get('domaine',array(),array(),array(array('etat','=','Validé')));
                        ?>
                        <form method="POST" action="../functions/traitementMonProfil.php">
                            <select name="selectDomaine" class="form-select">
                            <?php
                            foreach($domaine as $value){
                                echo '<option value="'.$value['iddomaine']. '">' .$value['libelle']. '</option>';
                            }
                            ?>
                            </select>
                            <button class="btn btn-outline-info" name="valider">Choisir</button>
                        </form>
                    </div>
                    <div class='col jumbotron'>
                        <div>Supprimer un domaine d'intérêt</div>
                   
                        <?php
                        // On affiche les noms domaines préférés de l'utilisateur
                        $libelleDomaineSuppr = $bdd->get('domaine',array('domaine.iddomaine','libelle'),array(array('INNER','abonne_domaine','domaine.iddomaine','abonne_domaine.iddomaine')),array(array('abonne_domaine.idabonne','=',$_SESSION['id'])));
                        ?>
                        <form method="POST" action="../functions/traitementMonProfil.php">
                            <select name="selectDomaineSuppr" class="form-select">
                            <?php
                            foreach($libelleDomaineSuppr as $value){
                                echo '<option value="'.$value['iddomaine']. '">' .$value['libelle']. '</option>';
                            }
                            ?>
                            </select>
                            <button class="btn btn-outline-info" name="supprimer">Supprimer</button>
                        </form>
                    </div>

                    <div class='col jumbotron'>
                        <div>Suggérer un nouveau domaine d'intérêt</div>
                        <form method="POST" action="../functions/traitementMonProfil.php">
                            <div class="row">
                                <div class="col">
                                    <input type="text" class="form-control form-control-sm" placeholder="Nom de domaine" name="newLiblDomaine" id="newLiblDomaine" required/>
                                </div>
                                <div class="col">
                                    <button class="btn btn-outline-info" name="ajouterNewDomaine">Suggérer</button>
                                </div>
                            </div>   
                        </form>
                        <?php if(isset($_GET['msgDomaine'])){
                                if($_GET['msgDomaine'] == "success"){
                                    echo '<span>Votre nom de domaine a été soumis !</span>';
                                }else if($_GET['msgDomaine'] == "fail"){
                                    echo '<span>Ce nom de domaine existe déjà.</span>';
                                }
                        } ?>
                    </div>
                </div>

            <?php   
                }
                if($_SESSION['connected'] == 1){
                    //echo '<p>Pseudo = ' .$pseudo. '<br> Mail = ' .$mailAdmin. '</p>';
            ?>
                <div class='jumbotron'>
                    <div>Modifier ses informations personnelles</div>
                    <form method="POST" class="box" action="../functions/traitementMonProfil.php">
                        <div class='row'>
                            <div class='col'> 
                                <input type="email" title='Format : example@gmail.com' class="form-control form-control-sm" pattern=".+@.+\..+" name="mail" id="mail" value=<?php echo $mailAdmin; ?> /><br>
                            </div>
                            <div class='col'>
                                <input type="text" class="form-control form-control-sm" value=<?php echo $nbNews; ?> name="nbNews" id="nbNews"/><br>
                            </div>
                            <div class='col'>                        
                                <input type="text" class="form-control form-control-sm" value=<?php echo $percentPubliNews; ?> name="percentPubli" id="percentPubli"/><br>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col'> 
                                <input type="password" class="form-control form-control-sm" placeholder="Modifier votre mot de passe" name="passwordAdmin" id="passwordAdmin"/><br>
                            </div>
                            <div class='col'>                     
                                <input type="password" class="form-control form-control-sm" placeholder="Confirmation du mdp" name="password_confirm_admin" id="password_confirm_admin"/><br>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-outline-success" name="submitAdmin" value="Valider"/>
                    </form>

                    <?php
                        if(isset($_GET['msgadm'])){
                            if($_GET['msgadm'] == "mail"){
                                echo '<span>Le mail est faux</span>';
                            }else if($_GET['msgadm'] == "mdp"){
                                echo '<span>Vous avez une erreur dans le mot de passe</span>';
                            }
                        }
                    ?>

                </div>
                <?php
                }
                ?>
        </div>
                
       
    
    </body>
</html>