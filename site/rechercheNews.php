    <?php
        include_once("header.php");
        if (isset($_POST['motcle'])) {
            $rechmotcle=$_POST['motcle'];
        }
        if (isset($_POST['nomdom'])) {
            $rechdomaine=$_POST['nomdom'];
        }
        if (isset($_POST['nomcat'])) {
            $rechcategorie=$_POST['nomcat'];
        }
        if (isset($_POST['archive'])) {
            $recharchive=$_POST['archive'];
        }
        if (isset($_POST['justif'])) {
            $rechjustif=$_POST['justif'];
        }

        //error_reporting(E_ERROR | E_PARSE);

    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <link rel="stylesheet" type="text/css" href="../style/rechercheNews.css">
        </head>
        <body>
            <body>
                <h2 class="text-center my-4">Rechercher parmi les News</h2>

                    <?php
                        //Utilisateur connecté, recherche par domaine, mot clé, archives et catégorie
                            
                            $motscles=$bdd->get('mot_cle',array('distinct nom'));
                            if ($_SESSION['connected']<>0) 
                                $domaines=$bdd->get('domaine',array('libelle'),array(),array(array('etat','=','Validé')));

                            //print_r($domaines);?>
                            <form action="rechercheNews.php" method="POST" class="box">
                                <div class="form-group row">
                                    <?php if ($_SESSION['connected']<>0) { ?>
                                    <label class="col-sm-2 col-form-label" for="nomDomaine">Domaine</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="nomdom" id="nomDomaine">
                                            <option value="">Choisir nom de domaine</option>
                                            <?php
                                            foreach($domaines as $domaine) {
                                                echo "<option value=\"".$domaine[libelle]."\">".$domaine[libelle]."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="nomCategorie">Catégorie</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="nomcat" id="nomCategorie">
                                            <option value="">Choisir nom de catégorie</option>
                                            <option value="valide">valide</option>
                                            <option value="non valide">non valide</option>
                                            <option value="fausse">fausse</option>
                                        </select>
                                    </div>
                                </div>
                                    
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label" for="motcle">Mot clé</label> <div class="col-sm-10">
                                        <select class="form-control" name="motcle" id="motCle">
                                            <option value="">Choisir un mot clé</option>";
                                            <?php
                                            foreach($motscles as $motcle){
                                                echo "<option value=\"".$motcle[nom]."\">".$motcle[nom]."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <?php if ($_SESSION['connected']<>0) { ?>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="estArchive" name="archive">
                                    <label class="form-check-label" for="estArchive">Recherche dans les news archivées</label>
                                    <?php } ?>
                                    <?php if ($_SESSION['connected']==1 || $_SESSION['connected']==2) { ?>
                                </div>
                                <?php if ($_SESSION['connected']<>1) { ?>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="estJustifie" name="justif">
                                    <label class="form-check-label" for="estJustifie">Recherche dans les news à justifier</label>
                                    <?php }} ?>
                                </div>
                                <button class="btn btn-primary" type="submit">Rechercher</button>
                            </form>
                                
                            <hr>
                
                                <?php
                                if ((!isset($_POST['motcle']) || empty($_POST['motcle'])) && !isset($_POST['nomdom']) && !isset($_POST['nomcat']) && !isset($_POST['archive']) && !isset($_POST['justif']) && $_SESSION['connected']<>0) {
                                    //$newsdomaineinteret=get("domaine",array('idnews','titre','date_publication'),array(array('INNER','news_abonne','news.idomaine','domaine.iddomaine')),array(array('WHERE',$_SESSION['id']))));
                                    $domainesinteret=$bdd->get("abonne_domaine",array('domaine.iddomaine','libelle'),array(array('INNER','domaine','domaine.iddomaine','abonne_domaine.iddomaine')),array(array('idabonne','=',$_SESSION['id'],'')));
                                    foreach ($domainesinteret as $d) {
                                        
                                        $newsdomaine=$bdd->get("news",array('news.idnews','date_publication','titre'),array(array('INNER','domaine','domaine.iddomaine','news.iddomaine')),array(array('domaine.iddomaine','=',$d['iddomaine'],'')),array('date_publication', 'DESC'),array(),array(),'3','');
                                        if (!empty($newsdomaine)) {
                                            echo "<p>domaine:".$d['libelle']."</p>";
                                        }
                                        echo '<div class="row ">';
                                        foreach ($newsdomaine as $news) {
                                            echo '<div class="col-sm-3 jumbotron card"><h3 class="card-title">id: '. $news['idnews'].' - titre: <a href=\'lireNews.php?news='.$news['idnews'].'\'>'. $news['titre'].'</a></h3><h4 card-subtitle mb-2 text-muted> publiée le '. $news['date_publication'].'</h4></div>';
                                        }
                                        echo '</div>';
                                    }
                                }
                                else {
                                ?>
                                <div class='filtres'>
                                    <p>filtres actuels:<p>
                                    <?php
                                    if (!empty($rechdomaine)) {
                                    echo "Nom domaine : ". $rechdomaine ."<br />";
                                    }
                                    if (!empty($rechcategorie)) {
                                    echo "Nom catégorie : ". $rechcategorie ."<br />";
                                    }
                                    if (!empty($rechmotcle)) {
                                        echo "Nom mot clé : ". $rechmotcle ."<br />";
                                    }
                                    if (!empty($rechjustif)) {
                                        echo "Recherche dans news à justifier<br />";
                                    }
                                    if (!empty($recharchive)) {
                                        echo "Recherche dans les archives<br />";
                                    }
                                    else {
                                        echo "Recherche dans les news en cours de validité";
                                    }
                                echo "</div>";
                    
                    if (!isset($recharchive)) {
                        if (!empty($rechcategorie)) {
                            $sqlcategorie=' WHERE categorie=\''.$rechcategorie.'\' ';
                        }
                        else  $sqlcategorie=' ';
                        if (!empty($rechdomaine)) {
                            if (!isset($rechcategorie) || empty($rechcategorie)) {
                                $sqldomaine1=' INNER JOIN domaine ON news.iddomaine=domaine.iddomaine ';
                                $sqldomaine2=' WHERE domaine.libelle=\''.$rechdomaine.'\' ';
                            }
                            else {
                                $sqldomaine1='INNER JOIN domaine ON news.iddomaine=domaine.iddomaine ';
                                $sqldomaine2=' AND domaine.libelle=\''.$rechdomaine.'\' ';
                            }
                        }
                        else {
                            $sqldomaine1=' ';
                            $sqldomaine2=' ';
                        }
                        if (!empty($rechmotcle)) {
                            if ((!isset($rechcategorie) || empty($rechcategorie)) && (!isset($rechdomaine) || empty($rechdomaine))) {
                                $sqlmotcle1=' INNER JOIN news_mot_cle ON news_mot_cle.idnews=news.idnews 
                                INNER JOIN mot_cle ON mot_cle.idmotcle=news_mot_cle.idmotcle ';
                                $sqlmotcle2='WHERE mot_cle.nom=\''. $rechmotcle .'\' ';
                            }
                            else {
                                $sqlmotcle1=' INNER JOIN news_mot_cle ON news_mot_cle.idnews=news.idnews
                                INNER JOIN mot_cle ON mot_cle.idmotcle=news_mot_cle.idmotcle ';
                                $sqlmotcle2=' AND mot_cle.nom=\''. $rechmotcle .'\' ';
                            }
                        }
                        else {
                            $sqlmotcle1=' ';
                            $sqlmotcle2=' ';
                        }
                        if (!empty($rechjustif)) {
                            if ((!isset($rechcategorie) || empty($rechcategorie)) && (!isset($rechdomaine) || empty($rechdomaine)) && (!isset($rechmotcle) || empty($rechmotcle) )) {
                                if ($_SESSION['connected']=2) {
                                    $sqljustif=' WHERE idverificateur='.$_SESSION['id'].' AND categorie=\'non valide\' ';
                                }
                                else if ($_SESSION['connected']=1) {
                                    $sqljustif=' WHERE idverificateur='.$_SESSION['id'].' ';
                                }
                            }
                            else {
                                if ($_SESSION['connected']=2) {
                                    $sqljustif=' AND idverificateur='.$_SESSION['id'].' AND categorie=\'non valide\' ';
                                }
                                else if ($_SESSION['connected']=1) {
                                    $sqljustif=' AND idverificateur='.$_SESSION['id'].' ';
                                }
                            }
                        }
                        else {
                            $sqljustif=' ';
                        }

                        
                        $sql="SELECT DISTINCT news.idnews,titre,date_publication FROM news ". $sqldomaine1 . $sqlmotcle1 . $sqlcategorie . $sqldomaine2 . $sqlmotcle2 . $sqljustif . "ORDER BY date_publication DESC";
                        //echo $sql;
                        $newsfiltrees=$bdd->debug_requete_select($sql);
                        //print_r($newsfiltrees);
                        echo "<div class='news'>";
                            echo '<div class="row ">';
                                foreach ($newsfiltrees as $news) {
                                    echo '<div class="col-sm-5 jumbotron searchedCard"><h3 class="card-title">id: '. $news['idnews'].' - titre: <a href=\'lireNews.php?news='.$news['idnews'].'\'>'. $news['titre'].'</a></h3><h4 card-subtitle mb-2 text-muted> publiée le '. $news['date_publication'].'</h4></div>';
                                }
                            echo '</div>';
                        echo "</div>";
                    }
                        //$newsfiltrees=$bdd->get('news',array('idnews','titre'),array(array('INNER','domaine','domaine.idDomaine','news.idDomaine'), 
                        //array('INNER','news_mot_cle','news_mot_cle.idnews','news.idnews'),
                        //array('INNER','mot_cle','mot_cle.idmotcle','news_mot_cle.idmotcle')),
                        //array(array('categorie','=',$rechcategorie,'AND'),
                        //array('mot_cle.nom','=',$rechmotcle,'AND'),
                        //array('domaine.libelle','=',$rechdomaine)));
                    
                    //Reacherche dans les news archivées
                    else if (!isset($rechjustif)) {
                        if (!empty($rechcategorie)) {
                            $sqlcategorie=' WHERE categorie=\''.$rechcategorie.'\' ';
                        }
                        else  $sqlcategorie=' ';
                        if (!empty($rechdomaine)) {
                            if (!isset($rechcategorie) || empty($rechcategorie)) {
                                $sqldomaine1=' INNER JOIN domaine ON news_archivee.iddomaine=domaine.iddomaine ';
                                $sqldomaine2=' WHERE domaine.libelle=\''.$rechdomaine.'\' ';
                            }
                            else {
                                $sqldomaine1='INNER JOIN domaine ON news_archivee.iddomaine=domaine.iddomaine ';
                                $sqldomaine2=' AND domaine.libelle=\''.$rechdomaine.'\' ';
                            }
                        }
                        else {
                            $sqldomaine1=' ';
                            $sqldomaine2=' ';
                        }
                        if (!empty($rechmotcle)) {
                            if (!isset($rechcategorie) || empty($rechcategorie) && !isset($rechdomaine) || empty($rechdomaine)) {
                                $sqlmotcle1=' INNER JOIN news_archivee_mot_cle ON news_archivee_mot_cle.idnewsarchivee=news_archivee.idnewsarchivee
                                INNER JOIN mot_cle ON mot_cle.idmotcle=news_archivee_mot_cle.idmotcle ';
                                $sqlmotcle2='WHERE mot_cle.nom=\''. $rechmotcle .'\' ';
                            }
                            else {
                                $sqlmotcle1=' INNER JOIN news_mot_cle ON news_archivee_mot_cle.idnewsarchivee=news_archivee.idnewsarchivee
                                INNER JOIN mot_cle ON mot_cle.idmotcle=news_archivee_mot_cle.idmotcle ';
                                $sqlmotcle2=' AND mot_cle.nom=\''. $rechmotcle .'\' ';
                            }
                        }
                        else {
                            $sqlmotcle1=' ';
                            $sqlmotcle2=' ';
                        }
                        $sql="SELECT DISTINCT news_archivee.idnewsarchivee,titre,date_publication FROM news_archivee ". $sqldomaine1 . $sqlmotcle1 . $sqlcategorie . $sqldomaine2 . $sqlmotcle2 . "ORDER BY date_publication DESC";
                        //echo $sql;
                        $newsfiltrees=$bdd->debug_requete_select($sql);
                        //print_r($newsfiltrees);

                        //affichage des news
                        foreach($newsfiltrees as $news){
                            echo '<div class="col-sm-5 jumbotron searchedCard"><h3 class="card-title">id: '. $news['idnewsarchivee'].' titre: <a href=\'lireNews.php?news='.$news['idnewsarchivee'].'\'>'. $news['titre'].'</a></h3><h4 card-subtitle mb-2 text-muted> publiée le '. $news['date_publication'].'</h4></div>';
                        }
                    } 
                }
                    ?>
                </body>
            </html>