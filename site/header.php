<?php
    session_start();
    include '../models/bdd.php';
    if(isset($_SESSION['pseudo']) && isset($_SESSION['pwd'])){
      Bdd::connectUser($_SESSION['pseudo'],$_SESSION['pwd']);
    }else{
      Bdd::connectSimpleUser();
      $_SESSION['connected']=0;
      $_SESSION['id']=null;
    }
    $bdd=Bdd::getBdd();
    //echo Bdd::$user;
    //echo Bdd::$pwd;

?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href="../style/connexion.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
        </script>
    </head>
    
    <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="accueil.php">Navigation</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="accueil.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="rechercheNews.php">Rechercher dans les news</a>
        </li>
        <?php if($_SESSION['connected'] != 0 && $_SESSION['connected'] != 1){?>
          <li class="nav-item">
            <a class="nav-link" aria-current="page" href="monProfil.php">Mon profil</a>
          </li>
        <?php } ?>
        
      </ul>
      
    
    </div>
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            <?php if($_SESSION['connected']==0){ ?>
            <a class="nav-link" aria-current="page" href="../functions/traitementInscriptionConnexion.php">
              Se connecter 
            </a>
            <?php }else{ ?>
            <a class="nav-link" aria-current="page" href="../functions/traitementInscriptionConnexion.php">
              Se déconnecter 
            </a>
            <?php } ?>
        </li>
    </ul>
  </div>
</nav>


