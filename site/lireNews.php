<?php
    include_once("header.php");

    if(isset($_GET['news'])){
        // Requête sélection news
        $news = $bdd->get('news',array(),array(array('INNER','domaine','news.iddomaine','domaine.iddomaine'),array('INNER','abonne','news.idredacteur','abonne.idabonne')),array(array('idnews','=',$_GET['news'])));
        $etatNews = 'news';
        // Si ce n'est pas une news, c'est une news archivée
        if(empty($news)) {
            $news = $bdd->get('news_archivee',array(),array(array('INNER','domaine','news_archivee.iddomaine','domaine.iddomaine'),array('INNER','abonne','news_archivee.idredacteur','abonne.idabonne')),array(array('idnewsarchivee','=',$_GET['news'])));
            $etatNews = 'news_archivee';
        }

        // Requête sélection mots clés
        if($etatNews == 'news') {
            $motCle = $bdd->get('mot_cle',array('nom'),array(array('INNER','news_mot_cle','mot_cle.idmotcle','news_mot_cle.idmotcle'),array('INNER','news','news_mot_cle.idnews','news.idnews')),array(array('news_mot_cle.idnews','=',$_GET['news'])));
        }
        if($etatNews == 'news_archivee') {
            $motCle = $bdd->get('mot_cle',array('nom'),array(array('INNER','news_archivee_mot_cle','mot_cle.idmotcle','news_archivee_mot_cle.idmotcle'),array('INNER','news_archivee','news_archivee_mot_cle.idnewsarchivee','news_archivee.idnewsarchivee')),array(array('news_archivee_mot_cle.idnewsarchivee','=',$_GET['news'])));
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>News</title>
    </head>
    
    <body>
        <header>
            <legend>
                <center><?php echo $news[0]['titre'] ?></center>
            </legend>  
        </header>
    
        <p>Publiée le <?php echo $news[0]['date_publication'] ?> par <?php echo $news[0]['pseudo'] ?>.</p>
        <table class="table table-striped table-hover">
            <tr>
                <td>Catégorie</td> 
                <td> <?php echo $news[0]['categorie'] ?></td>
            </tr>
            <tr>
                <td>Justification</td>
                <?php
                    if(($_SESSION['connected'] == 1 || ($_SESSION['connected'] == 2 && $_SESSION['id'] == $news[0]['idverificateur'] && $news[0]['categorie'] == 'non valide')) && $etatNews == 'news'){
                    ?>
                    <td>
                        <form method="POST" action="../functions/traitementLireNews.php?idnews=<?php echo $_GET['news']; ?>">
                            <div class="row">
                                <div class="col">
                                    <input type="text" class="form-control" id="justification" name="justification" value="<?php echo $news[0]['justification_etude'] ?>">
                                </div>
                                <div class="col">
                                    <select name="valideInvalide" class="form-select">
                                        <option value="valide">valide</option>
                                        <option value="fausse">fausse</option>
                                    </select>
                                    <button class="btn btn-success"  name="validerEtude">Valider</button>
                                </div>
                            </div>
                        </form>
                    </td>
                <?php } else{ ?>
                <td> <?php echo $news[0]['justification_etude'] ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>Domaine</td>
                <td> <?php echo $news[0]['libelle'] ?></td>
            </tr>
            <tr>
                <td>Mots clés :</td>
                <td><ul class="list-group">
                        <?php
                        foreach($motCle as $nomMotCle){
                            echo '<li class="list-group-item list-group-item-success">'.$nomMotCle['nom']. '</li>'; 
                        }
                        ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>Contenu</td>
                
                <?php if($_SESSION['id'] == $news[0]['idredacteur'] && $news[0]['categorie'] == 'non valide' && $etatNews == 'news') { ?>
                <td>
                    <form method="POST" action="../functions/traitementLireNews.php?idnews=<?php echo $_GET['news']; ?>">
                        <textarea name="texte" class="form-control" maxlength=300 type="text" cols="130" required><?php echo $news[0]['texte'] ?></textarea>
                        <div>
                            <input type="submit" class="btn btn-success" value="Modifier la news" name="modifier" />
                            <input type="submit" class="btn btn-warning" value="Supprimer la news" name="supprimer" />
                        </div>
                    </form>
                </td>
        
            <?php } else { ?>
                <td> <?php echo $news[0]['texte'] ?> </td>
            <?php } ?>
            </tr>
        </table>
        
    </body>
</html>