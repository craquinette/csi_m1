<?php
    session_start();
    //if (isset($_SESSION['connected'])) {
        $_SESSION['connected'] = 0;
        $_SESSION['id']=null;
        $_SESSION['pseudo'] = null;
        $_SESSION['pwd'] = null;
    //}

    require "../models/BddNews.php";
    //inscription();

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Connexion</title>
        <link rel="stylesheet" type="text/css" href="../style/connexion.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
        </script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
        </script>
    </head>
    
    <body>
        <header>
            <legend>
                <center>Bienvenue sur News.com !</center>
            </legend>  
        </header>




        <div id="contenu">
            <div id="connexion" class="jumbotron">
                <legend>
                    <center>Connexion</center>
                </legend>
                <form method="post" action="./../functions/traitementInscriptionConnexion.php">
                    <div class="form-group">
                        <?php
                            if (isset($_GET['loginerror']) && $_GET['loginerror']) {
                                echo "<span>Nom d'utilisateur ou mot de passe incorrect</span></br>";
                            }
                        ?>
                        <label for="loginC">Nom d'utilisateur</label>
                        <input type="text" class="form-control" id="loginC" name="loginC" maxlength="256" required>
                        <label for="mdpC" class="col-form-label">Mot de passe</label>
                        <input type="password" class="form-control" id="mdpC" name="mdpC" maxlength="256" required>
                    </div>
                    <button type="submit" class="btn btn-outline-success">Se connecter</button>
                </form>
            </div>

            <div id="inscription" class="jumbotron">
                <legend>
                    <center>Inscription </center>
                </legend>
                <form method="post" action="./../functions/traitementInscriptionConnexion.php">
                    <div class="form-group">
                        <label for="loginI">Nom utilisateur</label></br>
                        <?php
                            if (isset($_GET['erreur']) && $_GET['erreur']) {
                                echo "<span>Le nom d'utilisateur existe déjà.</span>";
                            }
                        ?>
                        <input type="text" class="form-control" id="loginI" name="loginI" minlenght="8" maxlength="256" required>
                        <label for="nom">Nom</label></br>
                        <input type="text" class="form-control" id="nom" name="nom" minlenght="8" maxlength="256" required>
                        <label for="prenom" class="col-form-label">Prénom</label>
                        <input type="text" class="form-control" id="prenom" name="prenom" maxlength="256" required><!-- gérer ça avec le min length -->
                        <label for="tel" class="col-form-label">Téléphone</label>
                        <input type="text" class="form-control" id="tel" name="tel" minlength="10" maxlength="10" required><!-- gérer ça avec le min length -->
                        <label for="mdpI" class="col-form-label">Mot de passe</label></br>
                        <input type="password" class="form-control" id="mdpI" name="mdpI" minlenght="8" maxlength="256" required>
                        <label for="confirmation" class="col-form-label">Confirmation du mot de passe</label></br>
                        <span id="identiqueCache">Le mot de passe de confirmation est différent du mot de passe</span> 
                        <input type="password" class="form-control" id="confirmation" name="confirmation" minlenght="8" maxlength="256" required>
                        <label for="animal" class="col-form-label">Animal</label>
                        <input type="text" class="form-control" id="animal" name="animal" maxlength="256" required><!-- gérer ça avec le min length -->

                    </div>
                    <button type="submit" id="btnInscription" class="btn btn-outline-success">S'inscrire</button>
                </form>
            </div>
        </div>

        <button type="submit" class="btn btn-primary btn-default btn-block" onclick="document.location='accueil.php'">Accéder au menu sans être connecté</button>

        <script type="text/javascript" src="./../js/connexion.js"></script>
    </body>
</html>