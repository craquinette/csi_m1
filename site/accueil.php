<?php
    include_once("header.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Accueil</title>
        <link rel="stylesheet" type="text/css" href="../style/styleAccueil.css">
    </head>
    
    <body>
        <header>
            <h1>Bienvenue sur News.com !</h1>
            <h2>Premier site de création et de lecture de news !</h2>
        </header>

        <?php
            // Requête sélection dernière news valide
            $idNewsValide = $bdd->get('news',array('max(idnews)'),array(),array(array('categorie','=','valide')))[0][0];
            $derniereNewsValide = $bdd->get('news',array('titre, date_publication'),array(),array(array('idnews','=',$idNewsValide)));
            
            // Requête sélection dernière news non valide
            $idNewsNonValide = $bdd->get('news',array('max(idnews)'),array(),array(array('categorie','=','non valide')))[0][0];
            $derniereNewsNonValide = $bdd->get('news',array('titre, date_publication'),array(),array(array('idnews','=',$idNewsNonValide)));
            
            // Requête sélection dernière news fausse
            $idNewsFausse = $bdd->get('news',array('max(idnews)'),array(),array(array('categorie','=','fausse')))[0][0];
            $derniereNewsFausse = $bdd->get('news',array('titre, date_publication'),array(),array(array('idnews','=',$idNewsFausse)));
        
            // Si une news vient d'être supprimée
            if(isset($_GET['info']) && $_GET['info'] == "suppr"){
                echo '<p>La news a bien été supprimée.</p>';
            }
        ?>  

        <div class="row">
            <!-- Affichage dernière news valide -->
        
            <div class="col jumbotron last_news">
            <h3 class="card-title">Dernière news valide :</h3>
            <div class="card-body">
        <?php
            echo '<h4 card-subtitle mb-2 text-muted>' .$derniereNewsValide[0]['titre']. '</h4>';
            echo '<p class="card-text"  >Publié le : ' .$derniereNewsValide[0]['date_publication']. '</p>';
            echo '<a class="card-link" href="lireNews.php?news=' .$idNewsValide. '">Consulter cette news</a></div></div>';
        ?>
            <!-- Affichage dernière news non valide -->
            <div class="col jumbotron last_news">
            <h3 class="card-title">Dernière news non valide :</h3>
            <div class="card-body">
        <?php
            echo '<h4 card-subtitle mb-2 text-muted>' .$derniereNewsNonValide[0]['titre']. '</h4>';
            echo '<p class="card-text">Publié le : ' .$derniereNewsNonValide[0]['date_publication']. '</p>';
            echo '<a class="card-link" href="lireNews.php?news=' .$idNewsNonValide. '">Consulter cette news</a></div></div>';
        ?>

            <!-- Affichage dernière news fausse -->
            <div class="col jumbotron last_news">
            <h3 class="card-title">Dernière news fausse :</h3>
            <div class="card-body">
        <?php
            echo '<h4 card-subtitle mb-2 text-muted>' .$derniereNewsFausse[0]['titre']. '</h4>';
            echo '<p class="card-text">Publié le : ' .$derniereNewsFausse[0]['date_publication']. '</p>';
            echo '<a class="card-link" href="lireNews.php?news=' .$idNewsFausse. '">Consulter cette news</a></div></div>';
        ?>
        </div>
        <hr>
        <?php

            if(!empty($_SESSION['id'])){
                echo '<div class="titre_news"><h3>Vos news :</h3>';
                
                $nbNewsEcrites = $bdd->get('news',array('COUNT(idnews)'),array(),array(array('idRedacteur','=',$_SESSION['id'])))[0][0];
                $nbNewsEcritesValide = $bdd->get('news',array('COUNT(idnews)'),array(),array(array('idRedacteur','=',$_SESSION['id'],'AND'),array('categorie','=','valide')))[0][0];
                $nbNewsEcritesNonValide = $bdd->get('news',array('COUNT(idnews)'),array(),array(array('idRedacteur','=',$_SESSION['id'],'AND'),array('categorie','=','non valide')))[0][0];
                $nbNewsEcritesFausse = $bdd->get('news',array('COUNT(idnews)'),array(),array(array('idRedacteur','=',$_SESSION['id'],'AND'),array('categorie','=','fausse')))[0][0];
                $infosNewsEcrites = $bdd->get('news',array('idnews','titre','categorie','date_publication','justification_etude'),array(),array(array('idRedacteur','=',$_SESSION['id'])),array(),$groupby = array('idnews'));
                
                echo '<p>Vous avez écrit ' .$nbNewsEcrites. ' news : </p>';
                echo '<p>' .$nbNewsEcritesValide. ' sont classées "Valide". ' .$nbNewsEcritesNonValide. ' n\'ont pas encore été étudiées (statut "Non valide"). ' .$nbNewsEcritesFausse. ' ont été classées "Fausse".</p>';
                ?>
                <p>Vous trouverez ci-dessous la liste des news que vous avez écrites avec leur classification :</p></div>
                
                
                <div class="row ">
                <?php
                foreach($infosNewsEcrites as $infos){
                    if($infos['justification_etude'] != '')
                        echo '<div class="col-sm-5 jumbotron card"><h3 class="card-title"><a class="card-link" href="lireNews.php?news=' .$infos['idnews']. '">' .$infos['titre']. '</a></h3><h4 card-subtitle mb-2 text-muted>Publiée le : ' .$infos['date_publication']. '</h4><div class="card-body"> Cette news a été classé comme <strong>' .$infos['categorie']. '</strong>.<br>Voici la justification de l\'étude : ' .$infos['justification_etude']. '</div></div>';
                    else
                    echo '<div class="col-sm-5 jumbotron card"><h3 class="card-title"><a class="card-link" href="lireNews.php?news=' .$infos['idnews']. '">' .$infos['titre']. '</a></h3><h4 card-subtitle mb-2 text-muted>Publiée le : ' .$infos['date_publication']. '</h4><div class="card-body"> Cette news a été classé comme <strong>' .$infos['categorie']. '</strong>.</div></div>';  
                }
                ?>
                </div><br>
                <hr>
                <?php
                    if (isset($_SESSION['connected']) AND $_SESSION['connected'] == 2){
                        echo '<h3 class="titre_news">News à étudier :</h3>';

                    }
                ?>
            <?php
            }
            
            // Affichage des news à étudier pour l'abonné de confiance
            if(isset($_SESSION['connected']) AND $_SESSION['connected'] == 2){
                $nbNewsEcrites = $bdd->get('news',array('COUNT(idnews)'),array(),array(array('idRedacteur','=',$_SESSION['id'])))[0][0];
                $newsAEtudier = $bdd->get('news',array(),array(),array(array('categorie','=','non valide','AND'), array('idverificateur','=',$_SESSION['id'])),array(),$groupby = array('idnews'),array(), '3');
                if (count($newsAEtudier) == 0){
                    echo '<p>Aucune news à étudier.</p>';
                }
                ?>
                <p class="titre_news">Vous pouvez étudier la liste de news ci-dessous pour les valider ou les invalider : </p>
                <?php
                foreach($newsAEtudier as $newsX){
                    $libelleDomaine = $bdd->get('domaine',array('libelle'),array(array('INNER','news','domaine.iddomaine','news.iddomaine')),array(array('idnews','=',$newsX['idnews'])))[0][0];
                    
                    $motCle = $bdd->get('mot_cle',array('nom'),array(array('INNER','news_mot_cle','mot_cle.idmotcle','news_mot_cle.idmotcle'),array('INNER','news','news_mot_cle.idnews','news.idnews')),array(array('news_mot_cle.idnews','=',$newsX['idnews'])));
                    echo '<div class="col-sm-3 jumbotron card"><h3 class="card-title"><p><strong>' .$newsX['titre']. '</strong></h3><h4 card-subtitle mb-2 text-muted> écrite le : ' .$newsX['date_publication']. '.</p></h4>';
                    echo '<div class="card-body"><p>' .$newsX['texte']. '</p>';
                    echo '<p>Domaine : ' .$libelleDomaine. '.</p>';
                    ?>
                    <p>Mots-clés :</p>
                    <ul>
                    <?php
                    foreach($motCle as $nomMotCle){
                        echo '<li>'.$nomMotCle['nom']. '</li>'; 
                    }
                    ?>
                    </ul>
                    <form method="POST" action="../functions/traitementValidationNews.php">
                        <?php echo '<input type="hidden" id="idnewsvalidee" name="idnewsvalidee" value =' .$newsX['idnews']. '>'; ?>
                        <label for="justification">Justification étude</label>
                        <input type="text" id="justification" name="justification"><br>
                        <select name="valideInvalide">
                            <option value="valide">valide</option>
                            <option value="fausse">fausse</option>
                        </select><br>
                        <button name="validerEtude">Valider</button><br></div></div>
                    </form>
                <?php
                }
                ?>
            <?php   
            }

            // Affichage des news à étudier pour l'admin
            if(isset($_SESSION['connected']) AND ($_SESSION['connected'] == 1 )){
                $nbNewsEcrites = $bdd->get('news',array('COUNT(idnews)'),array(),array(array('idadministrateur','=',$_SESSION['id'])))[0][0];
                $newsAEtudier = $bdd->get('news',array(),array(),array(array('categorie','=','non valide','AND'), array('idadministrateur','=',$_SESSION['id'])),array(),$groupby = array('idnews'),array(), '3');
                ?>
                <p class="titre_news">Vous pouvez étudier la liste de news ci-dessous pour les valider ou les invalider : </p>
                <?php
                foreach($newsAEtudier as $newsX){
                    $libelleDomaine = $bdd->get('domaine',array('libelle'),array(array('INNER','news','domaine.iddomaine','news.iddomaine')),array(array('idnews','=',$newsX['idnews'])))[0][0];
                    
                    $motCle = $bdd->get('mot_cle',array('nom'),array(array('INNER','news_mot_cle','mot_cle.idmotcle','news_mot_cle.idmotcle'),array('INNER','news','news_mot_cle.idnews','news.idnews')),array(array('news_mot_cle.idnews','=',$newsX['idnews'])));
                    echo '<div class="col-sm-3 jumbotron card"><h3 class="card-title"><p><strong>"' .$newsX['titre']. '"</strong></h3><h4 card-subtitle mb-2 text-muted>écrite le : ' .$newsX['date_publication']. '.</p></h4>';
                    echo '<div class="card-body"><p>' .$newsX['texte']. '</p>';
                    echo '<p>Domaine : ' .$libelleDomaine. '.</p>';
                    ?>
                    <p>Mots-clés :</p>
                    <ul>
                    <?php
                    foreach($motCle as $nomMotCle){
                        echo '<li>'.$nomMotCle['nom']. '</li>'; 
                    }
                    ?>
                    </ul>
                    <form method="POST" action="../functions/traitementValidationNews.php">
                        <?php echo '<input type="hidden" id="idnewsvalidee" name="idnewsvalidee" value =' .$newsX['idnews']. '>'; ?>
                        <label for="justification">Justification étude</label>
                        <input type="text" id="justification" name="justification"><br>
                        <select name="valideInvalide">
                            <option value="valide">valide</option>
                            <option value="fausse">fausse</option>
                        </select><br>
                        <button name="validerEtude">Valider</button><br></div></div>
                    </form>
                <?php
                }
                ?>
            <hr>
            <?php   
            }

            // Partie dans laquelle l'administrateur valide un nom de domaine
            if(isset($_SESSION['connected']) AND $_SESSION['connected'] == 1){
                $domaineAEtudier = $bdd->get('domaine',array(),array(),array(array('etat','=','En attente')));
                echo '<p class="titre_news">Vous trouverez ici la liste des noms de domaine à étudier :</p>';
                foreach($domaineAEtudier as $newNomDomaine){
                    echo '<ul>';
                        echo '<li>' .$newNomDomaine['libelle']. '</li>';
                        echo '<form method = "POST" action ="">';
                            echo '<button name = "validerNomDomaine' .$newNomDomaine['libelle']. '">Valider ce domaine</buttom>';
                            echo '<button name = "refuserNomDomaine' .$newNomDomaine['libelle']. '">Refuser ce domaine</buttom>';
                        echo '</form>';
                    echo '</ul>';
                    if(isset($_POST['validerNomDomaine' .$newNomDomaine['libelle']])){
                        $domaine = $bdd->update('domaine',array(array("etat","Validé")),array(array('libelle','=',$newNomDomaine['libelle'])));
                        // Rafraichissement de la page autrement le nom de domaine s'affiche tjrs même si l'on a validé
                        echo '<script type="text/JavaScript"> location.reload(); </script>';
                    }
                    if(isset($_POST['refuserNomDomaine' .$newNomDomaine['libelle']])){
                        $domaine = $bdd->update('domaine',array(array("etat","Non validé")),array(array('libelle','=',$newNomDomaine['libelle'])));
                        // Rafraichissement de la page autrement le nom de domaine s'affiche tjrs même si l'on a refusé
                        echo '<script type="text/JavaScript"> location.reload(); </script>';
                    }
                }
            }

        ?>
            <hr>
        <?php 
                
        if(!empty($_SESSION['id'])){
            echo '<h2 class="text-center my-4"> Ecrire une news</h2>
            <form method="POST" class="box" action="../functions/traitementEcritureNews.php">
                <div class="form-group form-control-lg">
                    <input type="text" class="form-control my-4" placeholder="Titre de la news" name="titre" id="titre" required/>

                    <textarea maxlength="300" type="text" class="form-control my-4" placeholder="Texte de la news" name="texteNews" id="texteNews" required></textarea>';

                    
                    $domaine = $bdd->get('domaine',array(),array(),array(array('etat','=','Validé')));
                    echo '<select class="form-control" name="selectDomaine"><option value="">--Domaine--</option>';
                    foreach($domaine as $value){
                        echo '<option value="'.$value['iddomaine']. '">' .$value['libelle']. '</option>';
                    }
                    echo '</select>';


                    echo '<div class="form-row">

                        <div class="my-4 form-group col-md-4">
                            <input type="text" class="form-control" placeholder="Premier mots-cles" name="mots_cles1" id="mots_cles1" required/>
                        </div>

                        <div class="my-4 form-group col-md-4">
                            <input type="text" class="form-control" placeholder="Deuxième mots-cles" name="mots_cles2" id="mots_cles2" required/>
                        </div>

                        <div class="my-4 form-group col-md-4">
                            <input type="text" class="form-control " placeholder="Troisième mots-cles" name="mots_cles3" id="mots_cles3" required/>
                        </div>

                    </div>

                    <input type="number" class="form-control" min="1" placeholder="Durée de publication" name="dureePubli" id="dureePubli" required/>

                    <div><input type="submit" class="btn btn-primary my-4" name="submitbut" value="Valider"/></div>

                </div>
            </form>';
        }
        ?>
        
    </body>
</html>