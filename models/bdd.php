<?php

    class Bdd
    {
        // Défini les attributs
        private static $server = 'pgsql:host=plg-broker.ad.univ-lorraine.fr';
        //private static $server = 'pgsql:host=localhost';
        private static $bdd = 'dbname=news_groupe_cefll';
        private static $port = 'port=5432';
        public static $user; //mettre deconnecte
        public static $pwd; //mettre deconnecte
        private static $bddConnection; //On stock la connexion à la bdd
        private static $bddObject = null; //On stock l'objet Bdd afin de créer une seule connexion à la bdd
        


        private function __construct()
        {
            try {
                Bdd::$bddConnection = new PDO(Bdd::$server . ';' . Bdd::$bdd . ';' . Bdd::$port . ';' . Bdd::$user. ';' . Bdd::$pwd); // Création d'une connexion à la base de données
                Bdd::$bddConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                //Bdd::$bddConnection->query("SET CHARACTER SET utf8");
                //date_default_timezone_set("Europe/Paris");
            } catch (PDOException $ex) {
                echo $ex->xdebug_message;
                //var_dump($ex);
                die('Impossible de joindre la base de données');
            }
        }
        // Obtention de la connexion à la base de données
        public static function getBdd()
        {

            if (Bdd::$bddObject == null) { // Si aucune connexion existe alors on en créé une sinon on retourne celle existante
                Bdd::$bddObject = new Bdd();
            }
            return Bdd::$bddObject;
        }

        //faire 3 fonctions : getbdd, connectSimpleUser,connectUser
        //getBdd reste tel quel
        public static function connectSimpleUser(){
            Bdd::$user = 'user=deconnecte';
            Bdd::$pwd = 'password=deconnecte';
            Bdd::$bddObject = null;
        }

        public static function connectUser($u,$p){
            Bdd::$user = 'user=' . $u;
            Bdd::$pwd = 'password=' . $p;
            Bdd::$bddObject = null;
        }

        public static function connectSystem(){
            Bdd::$user = 'user=syst';
            Bdd::$pwd = 'password=syst';
            Bdd::$bddObject = null;
        }


        //Fonction si problème dans la fonction get
        //exemple utilisation
        //$abonnes = $bdd->debug_requete_select('select * from abonne');
        public function debug_requete_select($req){
            $result = Bdd::$bddConnection->prepare($req, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $result->execute();
            return $result->fetchAll();
        }

  

         //Fonction qui réalise la majorité des select
         //exemples utilisation
         //$abonnes = $bdd->get('abonne',array('prenom','nom'),array(array('idAbonne','=','0','AND'),array('prenom','=','ernesto')),array(),$groupby = array('nom','prenom'),$having = array(array('nom','=','dillenschneider')));
         //$abonnes = $bdd->get('abonne',array('prenom','nom'),array(array('INNER','abonne_domaine','abonne.idabonne','abonne_domaine.idabonne')),array(array('abonne.idAbonne','=','0','AND'),array('prenom','=','erne')),array(),$groupby = array('nom','prenom'),$having = array(array('nom','=','dillen')));
         //$abonnes = $bdd->get('abonne',array(),array(array('idAbonne','=','0','AND'),array('prenom','=','ernesto','&AND')));
         // exemple avec LIMIT : $newsAEtudier = $bdd->get('news',array(),array(),array(array('categorie','=','non valide','AND'), array('idverificateur','=',$_SESSION['id'])),array(),$groupby = array('idnews'),array(), '3');
         // $abonnes = $bdd->get('abonne');
         public function get($table, $attributes = null,$jointure =null,$where = null, $orderby =null, $groupby=null, $having=null,$lowlimit=null,$highlimit=null)
        {
            $sql='SELECT ';

            //gestion des attributs
            if(empty($attributes)){
                $sql .= "*";
            }else{
                foreach($attributes as $attr){
                    $sql .= strtolower($attr);
                    $sql .= ",";
                }
                $sql = substr($sql, 0, -1);
            }
            $sql .= " ";

            //gestion de la table
            if($table == ""){
                return "Table non renseignée";
            }else{
                $sql .= " FROM ";
                $sql .= strtolower($table);
            }

            //gestion inner join
            if(!empty($jointure)){
                foreach($jointure as $j){
                    $sql .= " ";
                    $sql .= $j[0];
                    $sql .= " JOIN ";
                    $sql .= strtolower($j[1]);
                    $sql .= " ON ";
                    $sql .= strtolower($j[2]);
                    $sql .= " = ";
                    $sql .= strtolower($j[3]);
                }
                
            }

            //gestion de la clause where
            $sql .= $this->gestion_where_having($where,true);

            //gestion group by
            if(!empty($groupby)){
                $sql .= " GROUP BY ";
                foreach($groupby as $g){
                    $sql .= strtolower($g);
                    $sql .= ",";
                }
                $sql = substr($sql, 0, -1);
                $sql .= " ";

                //gestion having
                $sql .= $this->gestion_where_having($having,false);
            }
            
            //gestion order by 
            if(!empty($orderby) || $orderby != null){
                if($orderby[0] == "")
                    return "Champ order by non renseigné";
                if($orderby[1]  != "ASC" && $orderby[1]  != "DESC")
                    return "Order by DESC ou ASC uniquement accepté";
                $sql .= " ORDER BY ";
                $sql .= strtolower($orderby[0]) . " ";
                $sql .= $orderby[1];
            }

            //gestion LIMIT
            //amélioration possible offset
            if($lowlimit != null){
                $sql .= " LIMIT ";
                $sql .= $lowlimit;
                if($highlimit != null){
                    $sql .= ',';
                    $sql .= $highlimit;
                }        
            }

            //echo $sql; //important pour debug
            
           
            try{
                $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result->execute();
                return $result->fetchAll();
            }catch (PDOException $e) {
                die($e->getMessage());
            }
        }


        public function gestion_where_having($clause,$where){
            $sql="";
            if(!empty($clause)){
                $last = count($clause);
                $iterable = 0;

                if($where)
                    $sql .= " WHERE ";
                else if(!$where)
                    $sql .= " HAVING ";
                else
                    return "Renseigner booléen HAVING/WHERE";
                
                
                foreach($clause as $w){
                    $iterable++;
                    if((count($w)!=4) && (count($w)==3 && $last != $iterable)){
                        if($where){
                            return 'Tableau clause where incorrect';
                        }else{
                            return 'Tableau clause having incorrect';
                        }
                    }
                       
                    $sql .= strtolower($w[0]) . " ";
                    $sql .= $w[1] . " ";
                    $sql .= "E'" . $w[2] . "' ";
                    ///if(count($where)!=4)
                    if($last != $iterable)
                        $sql .= $w[3] . " ";
                    //$sql .= ",";
                }
                $sql = substr($sql, 0);
            }

            return $sql;
        }


        //Fonction pour réaliser des insertions
        //exemple utilisation
        //$bdd->insert('abonne',array(array("pseudo","lorrain"),array("prenom","lorrain"),array("nom","samson"),array("telephone","060660"),array("mdp","samsonsamson"),array("confiance","false")));
        public function insert($table, $attributes)
        {
            $sql = " INSERT INTO ";
            //gestion de la table
            if($table == "")
                return "Nom de table invalide";
            $sql .= strtolower($table);
            $sql .= "(";

            //gestion des attributs
            if(empty($attributes)){
                return "Veuillez renseigner des données";
            }else{
                foreach($attributes as $attr){
                    $sql .= strtolower($attr[0]);
                    $sql .= ",";
                }
                $sql = substr($sql, 0, -1);
                $sql .= ") VALUES(";
                foreach($attributes as $attr){
                    $sql .= "E'" . $attr[1] . "' ";
                    $sql .= ",";
                }
                $sql = substr($sql, 0, -1);
                $sql .= ")";
            }
            echo $sql;
            $sql .= " ";            
            try{
                $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                return $result->execute();
            }catch (PDOException $e) {
                die($e->getMessage());
            }
            
        }


        //Fonction pour réaliser des delete
        //Exemple utilisation
        //$abonnes = $bdd->delete('abonne',array(array('idAbonne','=','1','AND'),array('prenom','=','lorrain')));
        public function delete($table, $where=null)
        {
            $sql = "DELETE FROM ";
            //gestion de la table
            if($table == "")
                return "Nom de table invalide";
            $sql .= strtolower($table);

            //gestion du where
            $sql .= $this->gestion_where_having($where,true);

            //return $sql;
            try{
                $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                return $result->execute();
            }catch (PDOException $e) {
                die($e->getMessage());
            }
        }

        //Fonction pour réaliser des update
        //Exemple utilisation
        //$abonnes = $bdd->update('abonne',array(array("prenom","ern")),array(array('idAbonne','=','0','AND')));
        //$abonnes = $bdd->update('abonne',array(array("prenom","erne"),array("nom","dillen")),array(array('idAbonne','=','0','AND')));
        public function update($table, $attributes,$where=null)
        {
            $sql = "UPDATE ";
            //gestion de la table
            if($table == "")
                return "Nom de table invalide";
            $sql .= strtolower($table);

            //gestion des attributs
            $sql .= " SET ";
            if(empty($attributes))
                return "Pas d'attribut renseigné dans l'update";
            foreach($attributes as $attr){
                $sql .= strtolower($attr[0]);
                $sql .= "=";
                $sql .= "E'" . $attr[1] . "' ";
                $sql .= ',';
            }
            $sql = substr($sql, 0, -1);

            //gestion du where
            $sql .= $this->gestion_where_having($where,true);

            //echo $sql;
            try{
                $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                return $result->execute();
            }catch (PDOException $e) {
                die($e->getMessage());
            }
        }


        //$function à false si c'est une procédure
        public function appelFunction($nom,$parametres=null,$function=true){
            if($function)
                $sql = "SELECT ";
            else    
                $sql = "CALL ";
            $sql .= $nom;
            $sql .= "(";
            //a voir si ça passe aussi pour les int et pour les functions sans argument
            if($parametres != null){
                foreach($parametres as $p){
                    $sql .= "'";
                    $sql .=$p;
                    $sql .= "',";
                }
            }
            if($parametres <> null){
                $sql = substr($sql, 0, -1);
            }
            $sql .= ")";
            //return $sql;
            try{
                $result = Bdd::$bddConnection->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $result->execute();
                return $result->fetchAll();
            }catch (PDOException $e) {
                die($e->getMessage());
            }
        }
    }


?>