<?php
    require "bdd.php";
    

    function existePseudo($pseudo){
        Bdd::connectSimpleUser();
        $bdd = Bdd::getBdd();
        $result = $bdd->appelFunction("existePseudo",array($pseudo));
        return $result[0]['existepseudo'];
    }

    function connexion($pseudo,$mdp){
        Bdd::connectSimpleUser();
        $bdd = Bdd::getBdd();
        $result = $bdd->appelFunction("connexion",array($pseudo,$mdp));

        //return le chiffre représentant l'utilisateur
        //0 pour mdp/login incorrect
        return $result[0]['connexion'];
    }

    function deconnexion(){
        $bdd = Bdd::connectSimpleUser();

        //return le chiffre représentant l'utilisateur
        //0 pour mdp/login incorrect
        return 0;
    }

    function inscription($pseudo, $prenom, $nom, $tel, $mdp, $animal){
        //appeler avec 'mdp'
        Bdd::connectSimpleUser();
        $bdd = Bdd::getBdd();
        
        if(existePseudo($pseudo))
            return false;
        
        $result = $bdd->appelFunction("inscription",array($pseudo, $prenom, $nom, $tel, $mdp, $animal));
        
        
        

        //booléen si l'inscription s'est bien passée
        return $result[0]['inscription'];

    }


    function archivage(){
        Bdd::connectSystem();
        $bdd = Bdd::getBdd();
        $result = $bdd->appelFunction("archivageNews",null,false);
        return 0;
    }

    function passageAbonneConfiance(){
        Bdd::connectSystem();
        $bdd = Bdd::getBdd();
        $result = $bdd->appelFunction("boucleConfiance",null,false);
        return 0;
    }


    function changerMdp($pseudo,$oldmdp,$id,$newmdp,$abonne){
        Bdd::connectUser($pseudo,$oldmdp);
        $bdd = Bdd::getBdd();
        $result = $bdd->appelFunction("changerMdp",array($id,$newmdp,$abonne),false);
        return 0;
    }



    
?>