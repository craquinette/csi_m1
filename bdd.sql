CREATE TYPE categorie AS ENUM ('valide', 'non valide', 'fausse');
CREATE TYPE etatDomaine AS ENUM ('En attente', 'Validé', 'Non validé');


create table abonne(
	idAbonne serial primary key,
	pseudo varchar(30) unique not null,
	nom varchar(30) not null,
	prenom varchar(30) not null,
	animal_compagnie varchar(30) null,
	telephone varchar(10) not null,
	mdp varchar(30) not null check(length(mdp)>7),
	confiance boolean default false not null
);

--insert into abonne(pseudo,mdp) values ('craquinette','mdpmdpmdp')

create table administrateur(
	idAdministrateur serial primary key,
	pseudo varchar(30) unique not null,
	mail varchar(50) not null,
	mdp varchar(30) not null check(length(mdp)>7),
	nombre_news int not null,
	pourcentage_publication_validee real not null
);

create table domaine(
	idDomaine serial primary key,
	libelle varchar(50) not null,
	etat etatDomaine not null,
	idAdministrateur int null
);


alter table domaine add constraint FK_ADM foreign key (idAdministrateur) references administrateur(idAdministrateur);

create table mot_cle(
	idMotCle serial primary key,
	nom varchar(50) not null
);

create table news(
	idNews serial primary key,
	texte text not null,
	titre varchar(50) not null,
	date_publication date not null,
	categorie Categorie not null,
	justification_etude text null,
	idDomaine int not null,
	idRedacteur int not null,
	idVerificateur int null,
	idAdministrateur int null,
	nbjourpublication int not null
);

alter table news add constraint FK_DOM foreign key (idDomaine) references domaine(idDomaine);
alter table news add constraint FK_RED foreign key (idRedacteur) references abonne(idAbonne);
alter table news add constraint FK_VER foreign key (idVerificateur) references abonne(idAbonne);
alter table news add constraint FK_ADM foreign key (idAdministrateur) references administrateur(idAdministrateur);

create table news_mot_cle(
	idNews int,
	idMotCle int,
	primary key(idNews,idMotCle)
);

alter table news_mot_cle add constraint FK_NEW foreign key (idNews) references news(idNews);
alter table news_mot_cle add constraint FK_MOT foreign key (idMotCle) references mot_cle(idMotCle);

CREATE TABLE abonne_domaine(
    idDomaine int,
    idAbonne int,
    Primary key(idDomaine, idAbonne)
);

ALTER TABLE abonne_domaine ADD CONSTRAINT FK_DOM FOREIGN KEY (idDomaine) references domaine(idDomaine);
ALTER TABLE abonne_domaine ADD CONSTRAINT FK_ABO FOREIGN KEY (idAbonne) references abonne(idAbonne);

create table news_archivee(
	idNewsArchivee serial primary key,
	texte text not null,
	titre varchar(50) not null,
	date_publication date not null,
	categorie Categorie not null,
	justification_etude text null,
	idDomaine int not null,
	idRedacteur int not null,
	nbjourpublication int not null
);

alter table news_archivee add constraint FK_DOM foreign key (idDomaine) references domaine(idDomaine);
alter table news_archivee add constraint FK_RED foreign key (idRedacteur) references abonne(idAbonne);

create table news_archivee_mot_cle(
	idNewsArchivee int,
	idMotCle int,
	primary key(idNewsArchivee,idMotCle)
);

alter table news_archivee_mot_cle add constraint FK_NEW foreign key (idNewsArchivee) references news_archivee(idNewsArchivee);
alter table news_archivee_mot_cle add constraint FK_MOT foreign key (idMotCle) references mot_cle(idMotCle);

--role utilisateur
create role utilisateur;
grant select on news, news_mot_cle, domaine to utilisateur;
grant select on mot_cle to utilisateur;
grant select(idadministrateur,pseudo,mdp) on administrateur to utilisateur;
grant select on abonne to utilisateur;
grant insert(idabonne,pseudo,nom,prenom,animal_compagnie,telephone,mdp) on abonne to utilisateur;
GRANT USAGE ON SEQUENCE abonne_idabonne_seq TO utilisateur;

CREATE USER deconnecte WITH ENCRYPTED PASSWORD 'deconnecte' CREATEROLE;
GRANT utilisateur TO deconnecte;

--role abonne
create role abonne;
grant select on news,mot_cle,news_archivee,news_archivee_mot_cle,domaine,abonne_domaine to abonne;
grant select on abonne, news_mot_cle to abonne;
grant insert(idnews,texte,titre,date_publication,iddomaine,idredacteur,nbjourpublication) on news to abonne;
grant insert on domaine to abonne;
grant insert on news_mot_cle, mot_cle, abonne_domaine to abonne;
grant delete on abonne_domaine, news, news_mot_cle to abonne;
grant update(idnews, texte, titre, date_publication, iddomaine, idredacteur,nbjourpublication) on news to abonne;
grant update on abonne to abonne;
grant references on domaine,abonne,news,mot_cle to abonne;
GRANT USAGE ON SEQUENCE domaine_iddomaine_seq TO abonne;
GRANT USAGE ON SEQUENCE news_idnews_seq TO abonne;
GRANT USAGE ON SEQUENCE mot_cle_idmotcle_seq TO abonne;

--role abonne de confiance
create role abonne_confiance;
grant select on news,mot_cle,news_archivee,news_archivee_mot_cle,domaine, news_mot_cle, abonne, abonne_domaine to abonne_confiance;
grant insert on domaine to abonne_confiance;
grant insert on news_mot_cle, mot_cle, abonne_domaine, news to abonne_confiance;
grant delete on abonne_domaine, news, news_mot_cle to abonne_confiance;
grant update on news to abonne_confiance;
grant update on abonne to abonne_confiance;
grant references on domaine,abonne,news,mot_cle to abonne_confiance;
GRANT USAGE ON SEQUENCE domaine_iddomaine_seq TO abonne_confiance;
GRANT USAGE ON SEQUENCE news_idnews_seq TO abonne_confiance;
GRANT USAGE ON SEQUENCE mot_cle_idmotcle_seq TO abonne_confiance;

--role administrateur
create role administrateur;
grant select on news,mot_cle,news_archivee,news_archivee_mot_cle,domaine,administrateur, news_mot_cle, abonne_domaine to administrateur;
grant insert on news,domaine,administrateur,news_mot_cle to administrateur;
grant update on news to administrateur;
grant update on administrateur, domaine to administrateur;
grant references on domaine,abonne,news,mot_cle to administrateur;

CREATE USER admin_news WITH ENCRYPTED PASSWORD 'motdepasse';
GRANT administrateur TO admin_news;

--role systeme
create role systeme;
grant select on news,mot_cle,news_archivee,domaine,administrateur,abonne,news_mot_cle to systeme;
grant insert on news,news_archivee,news_archivee_mot_cle,news_mot_cle to systeme;
grant insert(confiance) on abonne to systeme;
grant update(confiance) on abonne to systeme;
grant update on news to systeme;
grant delete on news_mot_cle,news to systeme;
grant references on domaine,abonne,news,mot_cle to systeme;
GRANT USAGE ON SEQUENCE news_idnews_seq TO systeme;
GRANT USAGE ON SEQUENCE news_archivee_idnewsarchivee_seq TO systeme;

CREATE USER syst WITH ENCRYPTED PASSWORD 'syst' CREATEROLE;
GRANT systeme TO syst;




----------------------------------------------------------------------------------
--triggers

--vérifie que l'entité administrateur n'a qu'une seule instance
create or replace function unique_administrateur() returns trigger as $$
declare 
begin
if(select count(*) from administrateur) > 0 then
	raise exception 'Un seul administarteur autorisé';
end if;
return new;
end
$$ language plpgsql;

create trigger adm_unique_administrateur before insert on administrateur for each row
execute procedure unique_administrateur();


--verifie qu'une news contient au plu 3 mots clés
create or replace function trois_mots_cles_max_par_news() returns trigger as $$
declare 
begin
if(select count(*) from news_mot_cle where idnews=NEW.idnews) > 2 then
	raise exception '3 mot clés par news maximum';
end if;
return new;
end
$$ language plpgsql;

create trigger nmc_trois_mots_cles_max_par_news before insert or update on news_mot_cle for each row
execute procedure trois_mots_cles_max_par_news();


--Affecte un abonne de confiance à la vérification d'une news après sa rédaction
create or replace function news_redaction() returns trigger as $$
declare 
	abonne record;
	id_verificateur integer;
	nb_confiance integer;
	rand integer;
	compteur integer := 1;
begin
select count(*) into nb_confiance from abonne where confiance=true and idabonne <> NEW.idredacteur;
if nb_confiance = 0 then
	update news set idadministrateur = 1 where idnews = NEW.idnews;
else
	SELECT floor(random() * nb_confiance + 1)::int into rand;
	FOR abonne IN
		SELECT idabonne from abonne
		WHERE confiance = true
	LOOP
		--raise notice 'compteur %', compteur;
		if rand = compteur then
			update news set idverificateur = abonne.idabonne where idnews = NEW.idnews;
			exit;
		end if;
		compteur := compteur + 1;
	END LOOP;
end if;
return new;
end
$$ language plpgsql;

create trigger ne_news_redaction after insert on news for each row
execute procedure news_redaction();


----------------------------------------------------------------------------------
--functions/procedures

--verifie si le pseudo existe déjà
--renvoie true si c'est le cas
create or replace function existePseudo(login varchar) returns boolean as $$
declare 
begin
	if (select 1 from abonne where pseudo=login) is not null then
		return true;
		elsif (select 1 from administrateur where pseudo=login) is not null then
			return true;
			else return false;
	end if;
end
$$ language plpgsql;


--fonction de l'inscription. Elle renvoie l'id de l'abonne créé
create or replace function inscription(plogin varchar(30), pprenom varchar(30), pnom varchar(30), ptel varchar(10), pmdp varchar(100), panimal varchar(10)) returns integer as $$
declare
	amdp varchar(100);
begin
	if (select 1 from abonne where pseudo=plogin) is not null then
		raise exception 'Ce pseudo existe déjà';
	end if;
	if (select 1 from administrateur where pseudo=plogin) is not null then
		raise exception 'Ce pseudo existe déjà';
	end if;
	EXECUTE 'CREATE USER ' || plogin || E' WITH ENCRYPTED PASSWORD \'' || pmdp ||E'\'';
	EXECUTE 'GRANT abonne TO ' || plogin;
	insert into abonne(pseudo,prenom,nom,animal_compagnie,telephone,mdp) values(plogin,pprenom,pnom,panimal,ptel,pmdp);
	return max(idabonne) from abonne;
end
$$ language plpgsql;


--verifie la connexion, renvoie 0 si non connecté
create or replace function connexion(plogin varchar(30), pmdp varchar(100)) returns integer as $$
declare 
begin
	if (select idAdministrateur from administrateur where pseudo=plogin and mdp=pmdp) is not null then
		return 1;
		elsif (select idAbonne from abonne where pseudo=plogin and mdp=pmdp and confiance=true) is not null then
			return 2;
			elsif (select idAbonne from abonne where pseudo=plogin and mdp=pmdp and confiance=false) is not null then
				return 3;
			else return 0;
	end if;
end
$$ language plpgsql;


--verifie si l'abonne devient de confiance'
--renvoie true si c'est le cas
create or replace function devientConfiance(idA integer) returns boolean as $$
declare 
	nbNews integer; --parametre N
	pourcentagePublication real; --parametre V
	pourcentagePublicationUtilisateur real; --pourcentage calculé
	news_publiee integer; --nb news publié de l'utilisateur
	news_global integer; --nb news lié à l'utilisateur
begin
	select nombre_news,pourcentage_publication_validee into nbNews,pourcentagePublication from administrateur where idadministrateur=1 ;
	select count(*) into news_global from news where idredacteur=idA;
	
	if (news_global < nbNews) then
		return false;
	end if;
	
	select count(*) into news_publiee from news where idredacteur=idA and categorie='valide';
	pourcentagePublicationUtilisateur := (news_publiee::real/news_global::real);
	
	--raise notice 'nb news lié à lutilisateur %', news_global;
	--raise notice 'nb news publié de lutilisateur %', news_publiee;
	--raise notice 'ratio %', pourcentagePublicationUtilisateur;
	
	if (pourcentagePublicationUtilisateur>=pourcentagePublication) then
		return true;
	end if;
	return false;
end
$$ language plpgsql;


--boucle pour devenir abonne de confiance
create or replace procedure boucleConfiance() as $$
declare 
	abo record;
begin
	for abo in select * from abonne where confiance = false
	loop
		if (select devientConfiance(abo.idabonne)) then
			EXECUTE 'revoke abonne from ' || abo.pseudo;
			EXECUTE 'grant abonne_confiance to ' || abo.pseudo;
			update abonne set confiance=true where idabonne=abo.idabonne;
		end if;
	end loop;
end
$$ language plpgsql;


--boucle pour l'archivage des news
create or replace procedure archivageNews() as $$
declare 
	ne record;
	mo record;
	idNewsA integer;
begin
	for ne in 
		select * 
		from news n
		where n.date_publication + (n.nbjourpublication || ' day')::interval  < NOW()
	loop
		--raise notice 'n%',ne;
		insert into news_archivee(texte,titre,date_publication,categorie,justification_etude,iddomaine,idredacteur,nbjourpublication)
			values(ne.texte,ne.titre,ne.date_publication,ne.categorie,ne.justification_etude,ne.iddomaine,ne.idredacteur,ne.nbjourpublication);
		select max(idnewsarchivee) into idNewsA from news_archivee;
		for mo in 
			select * 
			from news_mot_cle 
			where news_mot_cle.idnews=ne.idnews
		loop
			--raise notice 'mo%',mo;
			insert into news_archivee_mot_cle(idnewsarchivee,idmotcle) values(idNewsA,mo.idmotcle);
			--select max(idnewsarchivee) into idNewsA from news_archivee;
			delete from news_mot_cle where idmotcle=mo.idmotcle;
			--le motcle n'est pas supprimé de la table motcle
		end loop;
		delete from news where idnews = ne.idnews;	
	end loop;
end
$$ language plpgsql;


--change le statut d'un abonné de confiance en abonne simple
create or replace procedure changementStatutConfiance(idA integer) as $$
declare 
	vuser varchar(100);
begin
	if(select count(*) from abonne where idabonne=idA and confiance=true) =0 then
		raise exception 'IdAbonne non valide';
	end if;
	select pseudo into vuser from abonne where idabonne=idA;
	EXECUTE 'revoke abonne_confiance from ' || vuser;
	EXECUTE 'grant abonne to ' || vuser;
	update abonne set confiance=false where idabonne=idA;
end
$$ language plpgsql;


--fonction à n'utiliser qu'en test, ne pas l'utiliser dans le code
--change le statut d'un abonné simple en abonne de confiance
create or replace procedure testPassageConfiance(idA integer) as $$
declare 
	vuser varchar(100);
begin
	if(select count(*) from abonne where idabonne=idA and confiance=false) =0 then
		raise exception 'IdAbonne non valide';
	end if;
	select pseudo into vuser from abonne where idabonne=idA;
	EXECUTE 'revoke abonne from ' || vuser;
	EXECUTE 'grant abonne_confiance to ' || vuser;
	update abonne set confiance=true where idabonne=idA;
end
$$ language plpgsql;


--changement de mdp
--abo est true si c'est un abonne sinon administrateur
create or replace procedure changerMdp(idA integer,pmdp varchar(100), abo boolean) as $$
declare 
	vuser varchar(100);
begin
	
	
	if abo then
		if (select count(*) from abonne where idabonne=idA) =0 then
			raise exception 'Abonné non valide';
		end if;
		select pseudo into vuser from abonne where idabonne=idA;
		EXECUTE 'ALTER USER ' || vuser || E' WITH PASSWORD\'' || pmdp ||E'\'';
		update abonne set mdp=pmdp where idabonne=idA;
	else 
		if (select count(*) from administrateur where idadministrateur=idA) = 0 then
			raise exception 'Administrateur non valide';
		end if;
		select pseudo into vuser from administrateur where idadministrateur=idA;
		EXECUTE 'ALTER USER ' || vuser || E' WITH PASSWORD\'' || pmdp ||E'\'';
		update administrateur set mdp=pmdp where idadministrateur=idA;
	end if;
end
$$ language plpgsql;